package com.challenge3binar

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.challenge3binar.databinding.FragmentKetigaBinding

class FragmentKetiga : Fragment() {

    private var _binding: FragmentKetigaBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentKetigaBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val aName = FragmentKetigaArgs.fromBundle(arguments as Bundle).name
        binding.tvName.text = "Nama $aName"
        findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData<Person>("person")
            ?.observe(viewLifecycleOwner) { (usia, alamat, job) ->
                binding.tvUsia.visibility = View.VISIBLE
                binding.tvUsia.text = "Usia : $usia"
                binding.tvAlamat.visibility = View.VISIBLE
                binding.tvAlamat.text = "Alamat : $alamat"
                binding.tvJob.visibility = View.VISIBLE
                binding.tvJob.text = "Pekerjaan : $job"
            }

        binding.btnF4.setOnClickListener{
            it.findNavController().navigate(R.id.action_fragmentKetiga_to_fragmentKeempat)
        }

    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}