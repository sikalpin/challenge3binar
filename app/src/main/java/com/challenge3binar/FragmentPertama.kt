package com.challenge3binar

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.challenge3binar.databinding.FragmentPertamaBinding

class FragmentPertama : Fragment() {

    private var _binding: FragmentPertamaBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
//        binding.btnF2.setBackgroundColor(Color.BLUE)
        // Inflate the layout for this fragment
        _binding = FragmentPertamaBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnF2.setOnClickListener{
            it.findNavController().navigate(R.id.action_fragmentPertama_to_fragmentKedua)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }


}