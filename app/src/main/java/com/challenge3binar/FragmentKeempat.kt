package com.challenge3binar

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.challenge3binar.databinding.FragmentKeempatBinding

class FragmentKeempat : Fragment() {

    private var _binding: FragmentKeempatBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentKeempatBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnBack.setOnClickListener{
            
            val usia = (binding.etUsia.text.toString()).toInt()
            fun ceknilai():String{
                return if(usia % 2 == 0){
                    "bernilai genap"
                }else{
                    "bernilai ganjil"
                }
            }
            val alamat = binding.etAlamat.text.toString()
            val job = binding.etJob.text.toString()
            val person = Person("$usia, ${ceknilai()}","$alamat","$job")

            findNavController().previousBackStackEntry?.savedStateHandle?.set("person",person)
            findNavController().navigateUp()
        }
    }
}